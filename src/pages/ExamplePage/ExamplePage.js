import React, { useEffect, useState } from "react";
import Input from "../../components/elements/Input/Input";
import Card from "../../components/elements/Card/Card";
import defaultData from "./products.json";
import SortIcon from "../../assets/sort.svg";

export default function ExamplePage() {
  const [input, setInput] = useState("");
  const [data, setData] = useState(defaultData);
  const [sorting, setSorting] = useState(0);

  useEffect(() => {
    if (input || sorting) {
      const filteredData = defaultData
        .filter((item) => item.name.toLowerCase().includes(input.toLowerCase()))
        .sort((a, b) => {
          if (sorting === 1) {
            return a.price - b.price;
          } else if (sorting === 2) {
            return b.price - a.price;
          }
          return true;
        });
      setData(filteredData);
      return;
    }
    setData(defaultData);
  }, [input, sorting]);

  const handleSorting = () => {
    setSorting(sorting + 1);
    if (sorting === 2) {
      setSorting(0);
      return;
    }
  };

  return (
    <main className="bg-[#F4F4F4] min-h-screen text-black p-4 md:px-60 md:py-12">
      <div className="flex justify-center">
        <Input input={input} setInput={setInput} />
      </div>
      <div className="flex justify-between md:justify-center items-center gap-x-6 mt-8">
        <div className="flex flex-col justify-center items-start md:items-center">
          <h1 className="font-bold text-2xl md:text-4xl">Products</h1>
          <p className="text-sm md:text-lg">{`${data.length} products found`}</p>
        </div>
        <img className="w-6 h-6" onClick={handleSorting} src={SortIcon} />
      </div>
      <div className="grid grid-cols-2 md:grid-cols-4 gap-4 mt-8">
        <Card items={data} />
      </div>
    </main>
  );
}
