import React from "react";
import SearchIcon from "../../../assets/search-icon.svg";

export default function Input({ input, setInput }) {
  return (
    <div className="relative w-full md:w-4/12 rounded-md">
      <img className="absolute top-[50%] translate-y-[-50%] w-4 h-4 left-3" src={SearchIcon} />
      <input className="w-full rounded-md px-10 py-2" onChange={(e) => setInput(e.target.value)} placeholder="Search Product" type="text" value={input} />
    </div>
  );
}
