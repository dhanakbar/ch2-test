import React from 'react';
import PropTypes from 'prop-types';

const price = (number) => {
  return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
};

export default function Card({ items }) {
  return (
    <>
      {items?.map((i, idx) => {
        return (
          <div className="" key={idx}>
            <div className="bg-[#FFFFFF] max-w-[220px] min-h-[320px] rounded-xl overflow-hidden p-3 md:p-4 relative border">
              <div className="flex justify-center">
                <img
                  className="w-full h-28 md:h-32 rounded-md object-cover"
                  src={i.image}
                />
              </div>
              <div className="mt-4">
                <h2 className="font-bold text-base md:text-xl mb-2">{i.name}</h2>
                <p className="text-sm md:text-base text-[#7C7A7A] line-clamp-2">
                  {i.description}
                </p>
              </div>
              <div className="absolute bottom-2">
                <h2 className="font-bold text-base md:text-xl mb-2">Rp {price(i.price)}</h2>
              </div>
            </div>
          </div>
        );
      })}
    </>
  );
}
