import React from 'react';
import PropTypes from 'prop-types';

export default function Layouts({ children }) {

  return (
    <>{children}</>
  );
}


Layouts.propTypes = {
  children: PropTypes.object.isRequired,
};
